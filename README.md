# Sito Italian Linux Society

Questo è il repository del sito https://www.ils.org generato staticamente con [Nikola](https://www.getnikola.com/).

## Sviluppare in locale con Docker

Scarica il repository. In seguito, da dentro la cartella principale:

```
# Aggiorna il tema (solo la prima volta)
git submodule update --init --recursive --force

# Avvio
docker-compose up --build

# Avvio (se non funziona sopra)
docker compose up --build
```

A questo punto visita questo indirizzo:

http://127.0.0.1:28000

Puoi cambiare la porta modificando il tuo file `.env`. Questa porta di default è stata scelta per evitarti collisioni se utilizzi altri repository Italian Linux Society. [Info](https://gitlab.com/ItalianLinuxSociety/ils-infrastructure/-/blob/main/DOCKER_GUIDELINES.md).

----

Se invece desideri generare solo la cartella `output` con Docker:

```
docker-compose run nikola build
```

Se invece NON desideri utilizzare Docker, vedi la prossima sezione.

## Avvio nativo

Requisiti di sviluppo per l'avvio nativo:

- avere un sistema operativo Unix-like qualsiasi
- python installato

Dipendenze per l'avvio nativo:

```
pip install Nikola[extras] pyyaml

git submodule update --init --recursive --force # scarica il tema di ILS

nikola build
nikola auto --browser
```

Per creare una singola build statica, eseguire questo comandi dalla cartella principale:

```
nikola build
```

## Segnalazioni

Per segnalazioni e richieste, apri una segnalazione:

https://gitlab.com/ItalianLinuxSociety/ils.org/-/issues/new

Oppure scrivi a:

webmaster@linux.it

Grazie!

## Licenza

Salvo ove diversamente indicato tutti i contenuti sono rilasciati in pubblico dominio, Creative Commons Zero.

https://creativecommons.org/publicdomain/zero/1.0/

Eccezioni: loghi di associazioni, di partner e sponsor. Contattarli per conoscere le relative licenze.
