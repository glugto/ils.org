<!--
.. title: 2017 con ILS
.. slug: 2017-con-ils
.. date: 2017-12-16 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. image_copy: 
.. previewimage:
-->

Come oramai ogni fine d'anno, una panoramica delle attività svolte da Italian Linux Society nei precedenti 12 mesi.

<ul>
  <li><a href="https://www.linux.it/unopercento">l'appello 1%</a>, destinato a sollecitare i professionisti che lavorano con il software libero a sostenere economicamente lo sviluppo e la crescita dei propri stessi strumenti di lavoro, è stato diffuso online <a href="{% link _posts/2017-03-16-1-per-100-coworking.md %}">ed offline</a>, riscontrando il favore di diversi operatori (anche a livello internazionale, tant'è che abbiamo dovuto tradurre la pagina dell'iniziativa <a href="https://www.linux.it/unopercento/en">in inglese</a>)</li>
  <li>un tema ricorrente quest'anno è stato quello del software libero nella pubblica amministrazione, cui abbiamo dedicato la campagna <a href="{% link _posts/2017-06-14-appello-per-i-comuni-condividete.md %}">Comune From Scratch</a> e per il quale abbiamo aderito all'analoga campagna europea <a href="{% link _posts/2017-09-27-publiccode.md %}">Public Code</a>. Ma molto, moltissimo lavoro resta ancora da fare su questo fronte...</li>
  <li>... a partire dal Ministero dell'Istruzione, presso cui <a href="{% link _posts/2017-06-09-software-libero-al-miur.md %}">ci siamo fatti vedere</a> (insieme agli altri soggetti che operano nel campo della promozione della cultura libera a scuola). L'incontro ed i successivi contatti hanno aiutato a migliorare le relazioni tra le community open/scolastiche ed il Ministero</li>
  <li><a href="https://www.linux.it/">linux.it</a>, primo punto d'accesso per centinaia di persone che quotidianamente vogliono saperne di più in merito a Linux e al software libero, è stato revisionato per rendere ancora più facile e chiara la navigazione. Tra le altre cose, è stata aggiunta una sintetica pagina che riassume <a href="https://www.linux.it/formatiaperti">i vantaggi dei formati aperti</a></li>
  <li>sempre su linux.it, ampio spazio è stato dedicato a coloro che sviluppano o vorrebbero sviluppare software libero: <a href="https://www.linux.it/opensource/howto">qualche dritta per chi vuole iniziare</a>, <a href="https://www.linux.it/opensource/licenze">una completa guida alle licenze opensource</a>, ed <a href="https://www.linux.it/freeware">una pagina espressamente dedicata agli sviluppatori di software freeware</a> (che speriamo di veder diventare freesoftware!)</li>
  <li>impossibile non citare infine il <a href="https://www.linuxday.it/2017/">Linux Day 2017</a>, la principale manifestazione nazionale dedicata a Linux, arrivata quest'anno alla diciassettesima edizione</li>
</ul>

Per restare sempre aggiornati su ciò che succede nel mondo del software libero in Italia potete sottoscrivere <a href="/newsletter">la nostra newsletter</a> o seguire <a rel="nofollow" href="https://twitter.com/ItaLinuxSociety/">il nostro account Twitter</a>. Nonché, ovviamente, associarvi alla nostra associazione e godere di <a href="/iscrizione">tutti i benefici riservati ai soci ILS</a>.