<!--
.. title: Open Data Day 2022
.. slug: opendataday2022
.. date: 2022-03-05 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. image_copy: <a rel="nofollow" href="https://commons.wikimedia.org/wiki/File:Open_Data_stickers.jpg">Jonathan Gray</a>, <a href="https://creativecommons.org/publicdomain/zero/1.0/deed.en">CC0</a>, via Wikimedia Commons
.. previewimage: /images/posts/opendata.jpg
-->


Il 5 marzo ricorre l'[Open Data Day](https://opendataday.org/), l'annuale celebrazione globale dei dati aperti.

<!-- TEASER_END -->

Per l'occasione segnaliamo un paio di iniziative legate a questo tema, che rientra nell'ampio spettro delle libertà digitali ma purtroppo troppo poco conosciuto nonostante le molteplici implicazioni culturali, economiche, sociali e politiche.

* [37100LAB](https://37100lab.comune.verona.it/), il laboratorio per l'innovazione del Comune di Verona, ha pubblicato [una serie di video-pillole introduttive](https://video.olos311.org/w/p/qSdnnybHwjtBFFDi3jsdfe) sull'argomento, da vedere, far vedere e condividere, per comprenderne ed approfondirne le diverse sfumature
* nel contesto della campagna [Dati Bene Comune](https://www.datibenecomune.it/), cui ILS ha aderito nel 2021, è stato lanciato un nuovo [appello alla trasparenza](https://vorrei.datibenecomune.it/dati-che-vorrei/) dei fondi allocati per il "Piano Nazionale di Ripresa e Resilienza" (PNRR) e all'urgenza di informare i cittadini su come e quando l'ingente mole di finanziamenti viene distribuita

Altre iniziative, in Italia e nel mondo, possono essere trovate sul sito dell'[Open Data Day](https://opendataday.org/).