<!--
.. title: Document Freedom Day: Dillo al Champion!
.. slug: document-freedom-day-dillo-al-champion
.. date: 2015-03-24 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. image_copy: 
.. previewimage:
-->

Mercoledi 25 marzo è il <a rel="nofollow" href="http://documentfreedom.org/">Document Freedom Day</a>, giornata internazionale dedicata ai formati digitali aperti e a tutto ciò che da essi consegue: interoperabilità, accessibilità, e libertà dal monopolio di specifici vendor. Temi da portare all'attenzione, in primis, delle pubbliche amministrazioni, tantopiù adesso che <a rel="nofollow" href="http://www.infodata.ilsole24ore.com/2015/03/11/il-miraggio-dellanagrafe-unica-piu-di-54mila-banche-dati-gestite-dalla-pa/">emerge preponderante</a> il problema dello scambio di dati tra diversi enti che usano diverse piattaforme software.

Con l'obiettivo di portare l'argomento appunto all'attenzione delle amministrazioni, soprattutto quelle locali, <a href="/">Italian Linux Society</a> e <a rel="nofollow" href="http://www.libreitalia.it/">LibreItalia</a> - l'associazione italiana di promozione a <a rel="nofollow" href="http://it.libreoffice.org/">LibreOffice</a>, che naturalmente del sostegno ai formati aperti fa la propria bandiera - chiamano all'appello i <a rel="nofollow" href="http://digitalchampions.it/">Digital Champions</a>, gli "ambasciatori digitali" reclutati da Riccardo Luna per consigliare, guidare e supportare gli enti pubblici nel loro percorso di digitalizzazione. E chiedono a tutti di fare altrettanto, sfruttando la nuova piattaforma "Dillo al Champion!", con la quale si può facilmente individuare il rappresentante più vicino e contattarlo mandandogli un messaggio su Twitter.

Gli strumenti per passare progressivamente ad una ampia adozione di formati liberi, documentati e trasparenti non mancano, ed i motivi - tecnici ed economici - abbondano. Quel che spesso manca è la volontà politica di farlo, e sta a noi sollecitare i diretti interessati.