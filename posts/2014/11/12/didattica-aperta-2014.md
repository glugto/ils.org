<!--
.. title: Didattica Aperta 2014
.. slug: didattica-aperta-2014
.. date: 2014-11-12 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. image_copy: 
.. previewimage:
-->

Il 29 novembre, presso il Dipartimento di Informatica dell'Università di Torino, si terrà l'edizione 2014 di <a rel="nofollow" href="http://www.didatticaaperta.it/">Didattica Aperta</a>, annuale convegno itinerante sullo stato dell'arte del software libero nella scuola.

Immancabile uno spazio dedicato alla <a rel="nofollow" href="http://wiildos.it/">comunità WiiLDOS</a>, la più grande ed attiva in Italia su questo tema, ma quest'anno in programma si trovano anche riferimenti a <a rel="nofollow" href="http://www.coderdojoitalia.org/">CoderDojo</a> (il gruppo internazionale che organizza workshop di programmazione di base per bambini) ed al Movimento Maker, che lentamente ma inarrestabilmente sta invadendo il mondo della didattica con stampanti 3D ed elettronica opensource.

Un breve intervento sarà rivolto anche a "La Libera Scuola", la risposta di Italian Linux Society all'<a rel="nofollow" href="http://labuonascuola.gov.it/">appello del Ministero per l'Istruzione</a> per raccogliere spunti e considerazioni sulla scuola italiana, ed al ruolo che la community linuxara nel suo insieme ha sull'innovazione nella didattica.

Sono invitati a partecipare in primis gli insegnanti che vorrebbero saperne di più sulle opportunità del software libero a scuola, ma anche studenti e genitori che vorrebbero contribuire a sostenere questa scelta.